import argparse
import pickle
import sys

import numpy as np
from matplotlib import pyplot
from tqdm import tqdm
from sklearn import metrics

from main import classify, load_mnist, Params, NUMBER_OF_CLASSES


def plot_cost(cost: list[float], accuracy: float) -> None:
    pyplot.clf()
    pyplot.plot(cost)
    pyplot.title(f'Accuracy: {accuracy:.2f}%')
    pyplot.xlabel('Batches')
    pyplot.ylabel('Cost')
    pyplot.legend(('Train cost',))
    pyplot.savefig('./accuracy.png')


def compute_confusion_matrix(
        test_data: np.ndarray,
        test_target: np.ndarray,
        params: Params) -> tuple[np.ndarray, list[tuple[int, np.ndarray]], float]:
    matrix: np.ndarray = np.zeros(shape=(NUMBER_OF_CLASSES, NUMBER_OF_CLASSES))
    sample: list[tuple[int, np.ndarray]] = []

    progressbar: tqdm = tqdm(range(len(test_data)), file=sys.stdout)
    correct: float = 0.0

    for iteration in progressbar:
        data: np.ndarray = test_data[iteration]

        output, confidence = classify(data, params)

        target: int = test_target[iteration]
        matrix[output][target] += 1
        sample.append((target, confidence))

        if output == target:
            correct += 1

        progressbar.set_description(f'Accuracy: {float(correct / (iteration + 1)) * 100:0.2f}%')

    return matrix.astype(np.int32), sample, correct / len(test_data)


Metrics = dict[str, float]


def compute_metrics(confusion_matrix: np.ndarray) -> list[Metrics]:
    model_metrics: list[Metrics] = []

    for cls in range(NUMBER_OF_CLASSES):
        tp: float = confusion_matrix[cls, cls]
        fp: float = confusion_matrix[cls, :].sum() - tp
        fn: float = confusion_matrix[:, cls].sum() - tp
        # tn: float = confusion_matrix.sum() - tp - fp - fn

        precision: float = tp / (tp + fp)
        recall: float = tp / (tp + fn)
        f1_score: float = 2 * precision * recall / (precision + recall)

        model_metrics.append(dict(
            precision=precision,
            recall=recall,
            f1_score=f1_score
        ))

    return model_metrics


def plot_metrics(model_metrics: list[Metrics]) -> None:
    precision: list[float] = [model_metrics[cls]['precision'] * 100 for cls in range(NUMBER_OF_CLASSES)]
    recall: list[float] = [model_metrics[cls]['recall'] * 100 for cls in range(NUMBER_OF_CLASSES)]
    f1_score: list[float] = [model_metrics[cls]['f1_score'] for cls in range(NUMBER_OF_CLASSES)]

    classes: list[int] = list(range(NUMBER_OF_CLASSES))

    pyplot.clf()
    pyplot.bar(classes, precision)
    pyplot.title('Precision')
    pyplot.xlabel('Class')
    pyplot.xticks(classes)
    pyplot.ylabel('Precision')
    pyplot.yticks(ticks=[0, 20, 40, 60, 80, 100], labels=[f'{it}%' for it in [0, 20, 40, 60, 80, 100]])
    pyplot.savefig('./precision.png')

    pyplot.clf()
    pyplot.bar(classes, recall)
    pyplot.title('Recall')
    pyplot.xlabel('Class')
    pyplot.xticks(classes)
    pyplot.ylabel('Recall')
    pyplot.yticks(ticks=[0, 20, 40, 60, 80, 100], labels=[f'{it}%' for it in [0, 20, 40, 60, 80, 100]])
    pyplot.savefig('./recall.png')

    pyplot.clf()
    pyplot.bar(classes, f1_score)
    pyplot.title('F-score')
    pyplot.xlabel('Class')
    pyplot.xticks(classes)
    pyplot.ylabel('F-score')
    pyplot.savefig('./f_score.png')


def compute_class_examples_count(test_target: np.ndarray) -> list[int]:
    counts: list[int] = [0 for _ in range(NUMBER_OF_CLASSES)]
    for i in range(len(test_target)):
        counts[test_target[i]] += 1

    return counts


def as_binary(
        cls: int,
        sample: list[tuple[int, np.ndarray]]) -> tuple[list[int], list[float]]:
    truth_labels: list[int] = [1 if cls == it[0] else 0 for it in sample]
    scores: list[float] = [it[1][cls] for it in sample]

    return truth_labels, scores


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('parameters', metavar='parameters', type=str)
    args = parser.parse_args()
    parameters_file_name: str = args.parameters

    with open(parameters_file_name, 'rb') as parameters_file:
        params, cost = pickle.load(parameters_file)

    cache_file_name: str = f'{parameters_file_name}-metrics-cache.pkl'
    try:
        with open(cache_file_name, 'rb') as cache_file:
            confusion_matrix, sample, accuracy, examples_count = pickle.load(cache_file)
    except FileNotFoundError:
        _, (test_data, test_target) = load_mnist()
        confusion_matrix, sample, accuracy = compute_confusion_matrix(test_data, test_target, params)
        examples_count: list[int] = compute_class_examples_count(test_target)

        with open(cache_file_name, 'wb') as cache_file:
            pickle.dump([confusion_matrix, sample, accuracy, examples_count], cache_file)

    plot_cost(cost, accuracy * 100)

    model_metrics: list[Metrics] = compute_metrics(confusion_matrix)

    plot_metrics(model_metrics)


if __name__ == '__main__':
    main()