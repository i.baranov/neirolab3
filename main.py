import argparse
import os
import pickle
import sys
import numpy as np
from tqdm import tqdm
import random

LEARNING_RATE: float = 0.01
NESTEROV_GAMMA: float = 0.95
BATCH_SIZE: int = 30
FIRST_LAYER: int = 5
FIRST_FILTERS: int = 7
SECOND_LAYER: int = 5
SECOND_FILTERS: int = 6
FULLY_CONNECTED_COUNT: int = 200
SOFT_MAX_COUNT: int = 10
NUMBER_OF_CLASSES: int = 10
IMAGE_CHANNELS_COUNT: int = 1
IMAGE_SIZE: int = 28

Params = list[np.ndarray]

Outputs = list[np.ndarray]

Gradients = list[np.ndarray]

Vs = list[np.ndarray]


def shuffle(data: np.ndarray, target: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
    indices: list[int] = list(range(len(data)))
    random.shuffle(indices)
    return data[indices], target[indices]


def load_mnist() -> tuple[tuple[np.ndarray, np.ndarray], tuple[np.ndarray, np.ndarray]]:
    import os
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    from keras.datasets import mnist
    (train_data, train_target), (test_data, test_target) = mnist.load_data()

    def prepare(array: np.ndarray) -> np.ndarray:
        return array.astype(np.float32).reshape((len(array), IMAGE_CHANNELS_COUNT, IMAGE_SIZE, IMAGE_SIZE)) / 255

    return (prepare(train_data), train_target), (prepare(test_data), test_target)


def create_filters(size: tuple[int, ...], scale: float = 1.0) -> np.ndarray:
    stddev: float = scale / np.sqrt(np.array(size).prod())
    return np.random.normal(loc=0, scale=stddev, size=size)


def cross_corr_bp(
        errors: np.ndarray,
        image: np.ndarray,
        filters: np.ndarray,
        stride: int) -> tuple[np.ndarray, np.ndarray]:
    filters_count, filter_channels, filter_size, _ = filters.shape
    _, image_size, _ = image.shape

    next_errors: np.ndarray = np.zeros_like(image)
    gradients: np.ndarray = np.zeros_like(filters)

    for filter_index in range(filters_count):
        filter: np.ndarray = filters[filter_index]
        gradient: np.ndarray = gradients[filter_index]
        error: np.ndarray = errors[filter_index]

        y: int = 0
        for offset_y in range(0, image_size - filter_size + 1, stride):
            x: int = 0
            for offset_x in range(0, image_size - filter_size + 1, stride):
                # evil math
                gradient += error[y, x] * image[:, offset_y:offset_y + filter_size, offset_x:offset_x + filter_size]
                next_errors[:, offset_y:offset_y + filter_size, offset_x:offset_x + filter_size] += error[y, x] * filter
                x += 1
            y += 1

    return next_errors, gradients


def argmax2d(arr: np.ndarray) -> tuple[int, int]:
    idx: int = arr.argmax()
    return idx // arr.shape[1], idx % arr.shape[1]


def maxpool_bp(
        errors: np.ndarray,
        image: np.ndarray,
        patch_size: int,
        stride: int) -> np.ndarray:
    channels_count, image_size, _ = image.shape

    next_errors: np.ndarray = np.zeros_like(image)

    for channel_index in range(channels_count):
        channel: np.ndarray = image[channel_index]
        channel_error: np.ndarray = next_errors[channel_index]
        error: np.ndarray = errors[channel_index]

        out_y: int = 0
        for curr_y in range(0, image_size - patch_size + 1, stride):
            out_x: int = 0
            for curr_x in range(0, image_size - patch_size + 1, stride):
                (y, x) = argmax2d(channel[curr_y:curr_y + patch_size, curr_x:curr_x + patch_size])
                channel_error[curr_y + y, curr_x + x] = error[out_y, out_x]
                out_x += 1
            out_y += 1

    return next_errors


def dense_bp(
        error: np.ndarray,
        layer: np.ndarray,
        layer_in: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
    grad = error.dot(layer_in.T)
    error = layer.T.dot(error)

    return error, grad


def compute_gradients(
        image: np.ndarray,
        target: np.ndarray,
        outputs: Outputs,
        params: Params) -> Gradients:
    conv1, conv2, fc1, fc2 = params
    conv1_out, conv2_out, downsampled, flattened, fc1_out, result = outputs

    error: np.ndarray = result - target

    error, fc2_grad = dense_bp(error, fc2, fc1_out)
    error[fc1_out <= 0] = 0

    error, fc1_grad = dense_bp(error, fc1, flattened)
    error = error.reshape(downsampled.shape)

    error = maxpool_bp(error, conv2_out, patch_size=2, stride=2)
    error[conv2_out <= 0] = 0

    error, conv2_grad = cross_corr_bp(error, conv1_out, conv2, stride=1)
    error[conv1_out <= 0] = 0

    _, conv1_grad = cross_corr_bp(error, image, conv1, stride=1)

    grads = [conv1_grad, conv2_grad, fc1_grad, fc2_grad]

    return grads


def cross_corr(
        image: np.ndarray,
        filters: np.ndarray,
        stride: int) -> np.ndarray:
    filters_count, filter_channels, filter_size, _ = filters.shape
    image_channels, image_size, _ = image.shape

    feature_map_size: int = (image_size - filter_size) // stride + 1
    feature_maps: np.ndarray = np.zeros(shape=(filters_count, feature_map_size, feature_map_size))

    for filter_index in range(filters_count):
        filter: np.ndarray = filters[filter_index]
        feature_map: np.ndarray = feature_maps[filter_index]

        y: int = 0
        for offset_y in range(0, image_size - filter_size + 1, stride):
            x: int = 0
            for offset_x in range(0, image_size - filter_size + 1, stride):
                feature_map[y][x] = \
                    (filter * image[:, offset_y:offset_y + filter_size, offset_x:offset_x + filter_size]).sum()
                x += 1
            y += 1

    return feature_maps


def maxpool(
        image: np.ndarray,
        patch_size: int,
        stride: int):
    image_channels, image_size, _ = image.shape
    downsampled_size: int = (image_size - patch_size) // stride + 1
    downsampled: np.ndarray = np.zeros(shape=(image_channels, downsampled_size, downsampled_size))

    for channel_index in range(image_channels):
        channel: np.ndarray = image[channel_index]
        downsampled_channel: np.ndarray = downsampled[channel_index]

        y: int = 0
        for offset_y in range(0, image_size - patch_size + 1, stride):
            x: int = 0
            for offset_x in range(0, image_size - patch_size + 1, stride):
                downsampled_channel[y, x] = \
                    channel[offset_y:offset_y + patch_size, offset_x:offset_x + patch_size].max()
                x += 1
            y += 1

    return downsampled


def softmax(z: np.ndarray) -> np.ndarray:
    exp: np.ndarray = np.exp(z)
    return exp / exp.sum()


def feed_forward(
        image: np.ndarray,
        params: Params) -> Outputs:
    conv1, conv2, fc1, fc2 = params

    conv1_out: np.ndarray = cross_corr(image, conv1, stride=1)
    conv1_out[conv1_out <= 0] = 0

    conv2_out: np.ndarray = cross_corr(conv1_out, conv2, stride=1)
    conv2_out[conv2_out <= 0] = 0
    downsampled: np.ndarray = maxpool(conv2_out, patch_size=2, stride=2)

    maps_count, map_size, _ = downsampled.shape
    flattened: np.ndarray = downsampled.reshape((maps_count * map_size * map_size, 1))

    fc1_out: np.ndarray = fc1.dot(flattened)
    fc1_out[fc1_out <= 0] = 0

    fc2_out: np.ndarray = fc2.dot(fc1_out)
    result: np.ndarray = softmax(fc2_out)

    return [conv1_out, conv2_out, downsampled, flattened, fc1_out, result]


def classify(image: np.ndarray, params: Params) -> tuple[int, np.ndarray]:
    result: np.ndarray = feed_forward(image, params)[-1]
    return result.argmax(), result


def initialize_weights(size: tuple[int, ...]) -> np.ndarray:
    return np.random.standard_normal(size=size) * 0.01


def to_one_hot(cls: int) -> np.ndarray:
    code: np.ndarray = np.zeros(shape=(NUMBER_OF_CLASSES, 1))
    code[cls][0] = 1.0
    return code


def cross_entropy(p: np.ndarray, q: np.ndarray) -> float:
    return -(q * np.log(p)).sum()


def update(
        params: Params,
        v: Vs,
        gradients: Gradients,
        gamma: float,
        lr: float,
        batch_size: int) -> None:
    for i in range(len(params)):
        v[i] = gamma * v[i] + lr * gradients[i] / batch_size
        params[i] -= v[i]


def create_accumulator(params: Params) -> Gradients:
    return [np.zeros_like(params[i]) for i in range(len(params))]


def accumulate_gradients(
        accumulator: Gradients,
        gradients: Gradients) -> None:
    for i in range(len(accumulator)):
        accumulator[i] += gradients[i]


def nesterov_gd(
        batch_data: np.ndarray,
        batch_target: np.ndarray,
        lr: float, gamma: float,
        params: Params,
        vs: Vs) -> (Params, Vs, float):
    cost: float = 0
    batch_size = len(batch_data)

    gradients: list[np.ndarray] = create_accumulator(params)

    for i in range(batch_size):
        data: np.ndarray = batch_data[i]
        target: np.ndarray = to_one_hot(batch_target[i])

        outputs = feed_forward(data, params)
        current_gradients: Gradients = compute_gradients(data, target, outputs, params)

        accumulate_gradients(gradients, current_gradients)

        cost += cross_entropy(outputs[-1], target)

    update(params, vs, gradients, gamma, lr, batch_size)

    cost = cost / batch_size

    return params, vs, cost


def train(
        lr: float,
        gamma: float,
        batch_size: int,
        parameters_file_name: str) -> None:
    (data, target), _ = load_mnist()

    batches: list[tuple[np.ndarray, np.ndarray]] = [
        (data[i:i + batch_size], target[i:i + batch_size])
        for i in range(0, len(data), batch_size)
    ]

    filters_first: np.ndarray = create_filters((FIRST_FILTERS, 1, FIRST_LAYER, FIRST_LAYER))  # (f. count, ch. count, width, height)
    filters_second: np.ndarray = create_filters((SECOND_FILTERS, FIRST_FILTERS, SECOND_LAYER, SECOND_LAYER))
    fc1: np.ndarray = initialize_weights((RELU_COUNT, 600))
    fc2: np.ndarray = initialize_weights((SOFT_MAX_COUNT, RELU_COUNT))

    params: Params = [filters_first, filters_second, fc1, fc2]

    cost: list[float] = []

    print(f'LR:{lr}, Batch Size:{batch_size}')

    vs: Vs = [np.zeros_like(filters_first), np.zeros_like(filters_second), np.zeros_like(fc1), np.zeros_like(fc2)]

    progressbar = tqdm(batches, file=sys.stdout)
    for batch in progressbar:
        data, target = batch
        params, vs, current_cost = nesterov_gd(data, target, lr, gamma, params, vs)
        cost.append(current_cost)
        progressbar.set_description(f'Cost: {current_cost:.2f}')

    with open(parameters_file_name, 'wb') as parameters_file:
        pickle.dump([params, cost], parameters_file)

def main() -> None:
    print('main')
    parser = argparse.ArgumentParser()
    parser.add_argument('parameters', metavar='parameters')
    args = parser.parse_args()
    parameters_file_name: str = args.parameters

    cache_file_name: str = f'{parameters_file_name}-metrics-cache.pkl'
    try:
        os.remove(cache_file_name)
    except FileNotFoundError:
        pass

    train(
         lr=LEARNING_RATE,
         gamma=NESTEROV_GAMMA,
         batch_size=BATCH_SIZE,
         parameters_file_name=parameters_file_name)


if __name__ == '__main__':
    main()
